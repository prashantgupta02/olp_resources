import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Title } from '@angular/platform-browser';

import { HttpClient } from "@angular/common/http";
import { first, retry } from "rxjs/operators";
import { AppConfig } from "./util/app.config";
import { NgbAccordionConfig, NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "app";
  resData: any = "";
  directoryPath: any = "";
  bookName: any;
  bookId: any;
  ANCILLARY_BOOK: any[] = [];
  SUBSECTION: any[] = [];
  Activity_Title: any = [];
  SELECTION: any[] = [];
  PDF_Resources: any[] = [];
  Spanish_Version: any[] = [];
  Spanish_AsnwerSheet: any[] = [];
  Answer_FileName: any[] = [];
  loading: any;
  termpArr = "";
  status: boolean = false;
  term: any;
  location: any;

  constructor(
    private appConfig: AppConfig,
    private http: HttpClient,
    private route: ActivatedRoute,
    private titleService: Title,
    config: NgbAccordionConfig
  ) {
    this.directoryPath = appConfig.getDirectoryPath();
    config.closeOthers = true;
    config.type = 'light';
  }
  ngOnInit() {

    this.loading = true;
    this.route.queryParams.subscribe(params => {
      if (params.book_id != null) {
        this.getFileData(params.book_id);
      }

    });
    this.location = window.location.host;
  }

  getFileData(book_id: any) {
    this.bookId = book_id;
    if (!book_id || book_id == null) {
      alert("User Id is not defined.");
      return;
    }
    return this.http
      .get(this.directoryPath + book_id + ".json")
      .subscribe(res => {
        if (res && res != null) {
          this.resData = res;
          this.bookName = this.resData[0].Book;
          this.setTitle(this.bookName);
          for (let i = 0; i < this.resData.length; i++) {
            this.ANCILLARY_BOOK.push(this.resData[i].ANCILLARY_BOOK);
          }
          this.ANCILLARY_BOOK = Array.from(new Set(this.ANCILLARY_BOOK));
        }
        this.loading = false;
        return res;

      });


  }
  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle + " - OLP Resources");
  }

  getTitle($event, type: string, ancillary?: any, subsection?: any, actTitle?: any) {
    $event.stopPropagation();
    if (type == "ancillary") {
      if (!this.termpArr || this.termpArr != $event.target.innerText) {
        this.termpArr = $event.target.innerText
        this.SUBSECTION = [];
      }

      for (let j = 0; j < this.resData.length; j++) {
        if (($event.target.innerText.trim()) == (this.resData[j].ANCILLARY_BOOK).trim()) {
          let bookFound = false;
          this.SUBSECTION.forEach((activityTitle, bookIndex) => {
            if (activityTitle == this.resData[j].SUBSECTION) {
              bookFound = true;
            }
          });
          if (!bookFound) {
            this.SUBSECTION.push(this.resData[j].SUBSECTION);
          }
        }
      }
      this.SUBSECTION.sort();

    } else if (type == "activity_Title") {
      this.status = true;
      if (!this.termpArr || this.termpArr != $event.target.innerText) {
        this.termpArr = $event.target.innerText
        this.Activity_Title = [];
      }

      for (let j = 0; j < this.resData.length; j++) {
        if ((ancillary.trim()) == (this.resData[j].ANCILLARY_BOOK.trim()) && ($event.target.innerText.trim()) == (this.resData[j].SUBSECTION).trim()) {
          let bookFound = false;
          this.Activity_Title.forEach((activityTitle, bookIndex) => {
            if (activityTitle == this.resData[j].Activity_Title) {
              bookFound = true;
            }
          });
          if (!bookFound) {
            this.Activity_Title.push(this.resData[j].Activity_Title);
          }
        }
      }
      this.Activity_Title.sort();
    } else if (type == "selection") {
      this.status = true;
      if (!this.termpArr || this.termpArr != $event.target.innerText) {
        this.termpArr = $event.target.innerText
        this.SELECTION = [];
      }
      for (let k = 0; k < this.resData.length; k++) {
        if ((ancillary.trim()) == (this.resData[k].ANCILLARY_BOOK.trim()) && (subsection.trim()) == (this.resData[k].SUBSECTION.trim()) && ($event.target.innerText.trim()) == (this.resData[k].Activity_Title.trim())) {
          let bookFound = false;
          this.SELECTION.forEach((selection, bookIndex) => {
            if (selection == this.resData[k].SELECTION) {
              bookFound = true;
            }
          });
          if (!bookFound) {
            this.SELECTION.push(this.resData[k].SELECTION);

          }
        }

      }
      this.SELECTION.sort(function (a, b) {
        if (a.trim() < b.trim()) { return -1; }
        if (a.trim() > b.trim()) { return 1; }
        return 0;
      })
    }

    else if (type == "PDF_Resource") {
      this.status = true;
      if (!this.termpArr || this.termpArr != $event.target.innerText) {
        this.termpArr = $event.target.innerText
        this.PDF_Resources = [];
      }
      for (let l = 0; l < this.resData.length; l++) {
        if ((ancillary.trim()) == (this.resData[l].ANCILLARY_BOOK.trim()) && (subsection.trim()) == (this.resData[l].SUBSECTION.trim()) && (actTitle.trim()) == (this.resData[l].Activity_Title.trim()) && ($event.target.innerText.trim()) == (this.resData[l].SELECTION.trim())) {
          this.PDF_Resources.length = 0;
          this.PDF_Resources.push({
            PDF: (this.resData[l].PDF).split(".")[0], Spanish_Version: (this.resData[l].Spanish_Version).split(".")[0], Spanish_AsnwerSheet: (this.resData[l].Spanish_AsnwerSheet).split(".")[0], Answer_FileName: (this.resData[l].Answer_FileName).split(".")[0]
          });


        }

      }
    }


  }

  prevent($event) {
    $event.stopPropagation();
  }

}
