import { Inject, Injectable } from '@angular/core';

@Injectable()
export class AppConfig {

  private directoryPath: any = 'assets/data/';
  constructor() { }

  public getDirectoryPath() {
    return this.directoryPath;
  }


}
