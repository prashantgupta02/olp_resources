import {Pipe, PipeTransform }from '@angular/core';

@Pipe( {
    name:'SearchFilter',
})
export class SearchFilter implements PipeTransform {
    transform(value:any, input:string) {
        if (input) {
            let inputLen = input.length;
            input = input.toLowerCase();
            let index:any = value.indexOf(input);
            if(index != -1){
              return value.filter(function (el:any) {
                if (el) {
                  console.log(el.substr(0, inputLen).toLowerCase().indexOf(input), el);
                    return el.substr(0, inputLen).toLowerCase().indexOf(input) > -1;
                }

            })
            }
        }
        return value;
    }
}
