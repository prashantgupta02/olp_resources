import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppConfig } from './util/app.config';
import { SearchFilter } from './util/searchPipe';

@NgModule({
  declarations: [
    AppComponent,
    SearchFilter
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    RouterModule.forRoot([{
      path: '',
      component: AppComponent
  },
])
  ],
  providers: [AppConfig, Title],
  bootstrap: [AppComponent]
})
export class AppModule { }
