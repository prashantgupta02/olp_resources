# HmhLandingPage

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/?book_id=`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { HttpClient } from "@angular/common/http";
import { first, retry } from "rxjs/operators";
import { AppConfig } from "./util/app.config";
import { NgbAccordionConfig, NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { SearchFilter } from './util/searchPipe'

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "app";
  param: any = "";
  resData: any = "";
  directoryPath: any = "";
  subSecPart: any;
  bookList: any[] = [];
  bookDetail: any[] = [];
  bookDetailArray: any[] = [];;
  bookName: any;
  ANCILLARY_BOOK: any[] = [];
  SUBSECTION: any[] = [];
  Activity_Title: any = [];
  SELECTION: any[] = [];
  PDF: any[] = [];
  Spanish_Version: any[] = [];
  Spanish_AsnwerSheet: any[] = [];
  Answer_FileName: any[] = [];

  constructor(
    private appConfig: AppConfig,
    private http: HttpClient,
    private route: ActivatedRoute,
    config: NgbAccordionConfig
  ) {
    this.directoryPath = appConfig.getDirectoryPath();
    config.closeOthers = true;
    config.type = 'light';
  }
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.param = params.book_id;
      if (this.param != null) {
        this.getFileData(this.param);
      }

    });

  }

  getFileData(book_id: any) {
    if (!book_id || book_id == null) {
      alert("User Id is not defined.");
      return;
    }
    return this.http
      .get(this.directoryPath + book_id + ".json")
      .subscribe(res => {
        if (res && res != null) {
          this.resData = res;
          this.bookDetailArray = [];

          for (let i = 0; i < this.resData.length; i++) {
            let bookFound = false;
            this.bookName = this.resData[i].Book;
            this.ANCILLARY_BOOK.push(this.resData[i].ANCILLARY_BOOK);
            this.SUBSECTION.push(this.resData[i].SUBSECTION);
            // this.Activity_Title.push(this.resData[i].Activity_Title);
            // this.SELECTION.push(this.resData[i].SELECTION);



            //let bookName = this.resData[i].Book;

            // this.bookList.forEach((bookDetail, bookIndex) => {
            //   if (bookDetail.bookName == bookName) {
            //     bookFound = true;
            //   }
            // });
            // if (bookFound) {
            //   continue;
            // }
            // for (let j = 0; j < this.resData.length; j++) {
            //   if (bookName == this.resData[j].Book) {
            //     let book = this.resData[j].Book;
            //     let ANCILLARY_BOOK = this.resData[j].ANCILLARY_BOOK;
            //     let SUBSECTION = this.resData[j].SUBSECTION;
            //     let Activity_Title = this.resData[j].Activity_Title;
            //     let SELECTION = this.resData[j].SELECTION;
            //     let PDF = this.resData[j].PDF;
            //     let Spanish_Version = this.resData[j].Spanish_Version;
            //     let Spanish_AsnwerSheet = this.resData[j].Spanish_AsnwerSheet;
            //     let ANSWER_FILENAME = this.resData[j].Answer_FileName;
            //     let index = -1;
            //     for (let i = 0; i < this.bookDetailArray.length; i++) {
            //       if (this.bookDetailArray[i].dataObj['ANCILLARYBOOK'].ANCILLARYBOOK == ANCILLARY_BOOK) {
            //         if (this.bookDetailArray[i].dataObj['ANCILLARYBOOK']['SUBSECTION'].SUBSECTION == SUBSECTION) {
            //           if (this.bookDetailArray[i].dataObj['ANCILLARYBOOK']['SUBSECTION']['ActivityTitle'].ActivityTitle == Activity_Title) {
            //             index = i;
            //           }
            //         }
            //       }
            //     }
            //     let obj = { bookTitle: '', dataObj: {} };
            //     if (index == -1) {
            //       obj = { bookTitle: book, dataObj: {} };
            //       obj.dataObj['ANCILLARYBOOK'] = { ANCILLARYBOOK: ANCILLARY_BOOK };
            //       obj.dataObj['ANCILLARYBOOK']['SUBSECTION'] = { SUBSECTION: SUBSECTION };
            //       obj.dataObj['ANCILLARYBOOK']['SUBSECTION']['ActivityTitle'] = { ActivityTitle: Activity_Title };
            //       obj.dataObj['ANCILLARYBOOK']['SUBSECTION']['ActivityTitle']['SELECTION'] = { 'SELECTION': SELECTION, objArr: [] };
            //     }
            //     if (index != -1) {
            //       obj = this.bookDetailArray[index];
            //     }
            //     obj.dataObj['ANCILLARYBOOK']['SUBSECTION']['ActivityTitle']['SELECTION'].objArr.push({ PDF: PDF });
            //     obj.dataObj['ANCILLARYBOOK']['SUBSECTION']['ActivityTitle']['SELECTION'].objArr.push({ Spanish_Version: Spanish_Version });
            //     obj.dataObj['ANCILLARYBOOK']['SUBSECTION']['ActivityTitle']['SELECTION'].objArr.push({ Spanish_AsnwerSheet: Spanish_AsnwerSheet });
            //     obj.dataObj['ANCILLARYBOOK']['SUBSECTION']['ActivityTitle']['SELECTION'].objArr.push({ ANSWER_FILENAME: ANSWER_FILENAME });
            //     if (index != -1) {
            //       this.bookDetailArray[index] = obj;
            //     } else {
            //       this.bookDetailArray.push(obj);
            //     }
            //   }

            // }
            // this.bookList.push({ bookName: bookName, bookDetails: this.bookDetailArray });
          }
          //console.log(this.bookDetailArray[0].dataObj.ANCILLARYBOOK.SUBSECTION);

          // this.BookId = Array.from(new Set(this.BookId));
          // this.AncillaryBook = Array.from(new Set(this.AncillaryBook));
          // this.Selection = Array.from(new Set(this.Selection));
          // this.SubSection = Array.from(new Set(this.SubSection));
          // this.ActivityTitle = Array.from(new Set(this.ActivityTitle));

          this.ANCILLARY_BOOK = Array.from(new Set(this.ANCILLARY_BOOK));
          this.SUBSECTION = Array.from(new Set(this.SUBSECTION));
          // this.Activity_Title = Array.from(new Set(this.Activity_Title));
          // this.SELECTION = Array.from(new Set(this.SELECTION));

        }
        //console.log(this.ANCILLARY_BOOK, this.SUBSECTION, this.Activity_Title, this.SELECTION);
        return res;
      });


  }



  getTitle($event, type: string, ancillary?: any, subsection?: any) {


    if (type == "activity_Title") {
      this.Activity_Title = [];
      for (let j = 0; j < this.resData.length; j++) {
        if (ancillary == this.resData[j].ANCILLARY_BOOK && $event.target.innerText == this.resData[j].SUBSECTION) {
          let bookFound = false;
          this.Activity_Title.forEach((ActivityTitle, bookIndex) => {
            if (ActivityTitle == this.resData[j].Activity_Title) {
              bookFound = true;
            }
          });
          if (!bookFound) {
            this.Activity_Title.push(this.resData[j].Activity_Title);
          }
        }
      }
      console.log(this.Activity_Title);
    }
    if (type == "selection") {
      this.SELECTION = [];
      for (let k = 0; k < this.resData.length; k++) {
        if (ancillary == this.resData[k].ANCILLARY_BOOK && subsection == this.resData[k].SUBSECTION && $event.target.innerText == this.resData[k].Activity_Title) {
          let bookFound = false;
          this.SELECTION.forEach((selection, bookIndex) => {
            if (selection == this.resData[k].SELECTION) {
              bookFound = true;
            }
          });
          if (!bookFound) {
            this.SELECTION.push(this.resData[k].SELECTION);
            this.Activity_Title;
          }
        }
      }
      console.log(this.SELECTION);
    }


  }

}


<!-- <ng-container *ngFor="let bookDetail of bookDetailArray; let i = index;">
    <ngb-accordion [closeOthers]="true" activeIds="static-1">
      <ngb-panel id="static-1{{i}}" title="{{bookDetail.dataObj.ANCILLARYBOOK.ANCILLARYBOOK}}">
        <ng-template ngbPanelContent>
          <ngb-accordion [closeOthers]="true" activeIds="static-2">
            <ngb-panel id="static-2{{i}}" title="{{bookDetail.dataObj.ANCILLARYBOOK.SUBSECTION.SUBSECTION}}">
              <ng-template ngbPanelContent>
                <ngb-accordion [closeOthers]="true" activeIds="static-3">
                  <ngb-panel id="static-3{{i}}" title="{{bookDetail.dataObj.ANCILLARYBOOK.SUBSECTION.ActivityTitle.ActivityTitle}}">
                    <ng-template ngbPanelContent>
                      <ngb-accordion [closeOthers]="true" activeIds="static-4">
                        <ngb-panel id="static-4{{i}}" title="{{bookDetail.dataObj.ANCILLARYBOOK.SUBSECTION.ActivityTitle.SELECTION.SELECTION}}">
                          <ng-template ngbPanelContent>
                            <ngb-accordion [closeOthers]="true" activeIds="static-5">
                              <ng-container *ngFor="let selection of bookDetail.dataObj.ANCILLARYBOOK.SUBSECTION.ActivityTitle.SELECTION.objArr; let j = index;">
                                <ngb-panel *ngIf="selection.PDF && selection.PDF != 'xx'" id="static-5{{j}}" title="Resource">
                                  <ng-template ngbPanelContent>
                                    <ul>
                                      <li>
                                        <a href="/assets/pdf/{{selection.PDF}}" id="activityTitle{{j}}" target="_blank">{{selection.PDF}}</a>
                                      </li>
                                    </ul>
                                  </ng-template>
                                </ngb-panel>
                                <ngb-panel *ngIf="selection.Spanish_Version && selection.Spanish_Version != 'xx'" id="static-6{{j+1}}" title="Spanish Version">
                                  <ng-template ngbPanelContent>
                                    <ul>
                                      <li>
                                        <a href="/assets/pdf/{{selection.Spanish_Version}}" id="activityTitle{{j+1}}" target="_blank">{{selection.Spanish_Version}}</a>
                                      </li>
                                    </ul>
                                  </ng-template>
                                </ngb-panel>
                                <ngb-panel *ngIf="selection.Spanish_AsnwerSheet && selection.Spanish_AsnwerSheet != 'xx'" id="static-7{{j+2}}" title="Spanish AsnwerSheet">
                                  <ng-template ngbPanelContent>
                                    <ul>
                                      <li>
                                        <a href="/assets/pdf/{{selection.Spanish_AsnwerSheet}}" id="activityTitle{{j+2}}" target="_blank">{{selection.Spanish_AsnwerSheet}}</a>
                                      </li>
                                    </ul>
                                  </ng-template>
                                </ngb-panel>
                                <ngb-panel *ngIf="selection.Answer_FileName && selection.Answer_FileName != 'xx'" id="static-8{{j+3}}" title="Answer FileName">
                                  <ng-template ngbPanelContent>
                                    <ul>
                                      <li>
                                        <a href="/assets/pdf/{{selection.Answer_FileName}}" id="activityTitle{{j+3}}" target="_blank">{{selection.Answer_FileName}}</a>
                                      </li>
                                    </ul>
                                  </ng-template>
                                </ngb-panel>
                              </ng-container>
                            </ngb-accordion>
                          </ng-template>
                        </ngb-panel>
                      </ngb-accordion>
                    </ng-template>
                  </ngb-panel>
                </ngb-accordion>
              </ng-template>
            </ngb-panel>
          </ngb-accordion>
        </ng-template>
      </ngb-panel>
    </ngb-accordion>
  </ng-container> -->